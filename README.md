# ECO32-Assembler-Interpreter

This project is an interpreter and debugger for the eco32-assembler code developed by [Prof. Dr. Hellwig Geisse](https://homepages.thm.de/~hg53/). The given program handles plain assembler-text files instead of binary files.

At the moment the most used functions and a memory-manager (which allows malloc(), realloc() and free() -functions, similar to C) are implemented.

If you want to try it build and run it with sbt:
```
$ sbt compile 
$ sbt "run-main ecosim.EcoSimulator <Options*> <Your assembler file>"
$ sbt "run-main ecosim.EcoSimulator test.asm"
```

Or for the graphical mode:
```
$ sbt compile 
$ sbt "run-main ecosim.interactive.InteractiveSimulator"
```

# Debugger Features
Our debugger has some interesting features:
* live editing the assembler
* live view of the memory
* live view of the registers
* set breakpoints at specific lines

# Project-structure
* asm-tests/ - assembler test files
* assets/assets/ - additionall ressources (similar to rsc in maven-projects)
* doc/ - the documentation
* project/ - configuration-files for sbt
* script/ - our start-scripts which makes it easy to use the published versions
* src/ - our source-code
* test/ - our test-code

# License
This project is published under the MIT-License.
For more information see the LICENSE -file.
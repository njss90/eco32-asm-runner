main:
sub $29, $20, -16 ;setup frame

add $8,$0,5       ;value for a
stw $8, $25, -4   ;save as local #1
add $8,$0,10      ;value for b
stw $8,$25, -8    ;save as local #2

;load values for if-else
ldw $8, $25, -4 ; value a
ldw $9, $25, -8 ; value b

bne $8,$9, thenLabel
  ; reached if conditions is false
  ldw $8, $25, -4 ;load value for a
  stw $8, $25, -12 ; store as value for c
  j continue ;jump over thenLabel

  ;reached if condition is true
  thenLabel:
  add $8, $0, 20 ;value for c
  stw $8,$25, -12

continue:


; ================ functions for printing the variables
;call printi
ldw $8, $25, -4 ;load value for a
stw $8, $29, 0 ;store as #1 out-arg
jal printi

  ;print whitespace
  add $8, $0, 0x20
  stw $8, $29, 0
  jal printc

ldw $8, $25, -8 ;load value for b
stw $8, $29, 0 ;store as #1 out-arg
jal printi

  ;print whitespace
  add $8, $0, 0x20
  stw $8, $29, 0
  jal printc

ldw $8, $25, -12 ;load value for c
stw $8, $29, 0 ;store as #1 out-arg
jal printi

  ;print line-break
  add $8, $0, 0xA
  stw $8, $29, 0
  jal printc

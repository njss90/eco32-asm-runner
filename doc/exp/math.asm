main:
sub $29, $20, -12 ;setup frame

add $8,$0,5 ;value for a
stw $8, $25, -4 ;save as local #1
add $8,$0,10 ;value for b
stw $8,$25, -8 ;save as local #2

;calculate value for c
ldw $8, $25, -4 ;load local #1 = a
ldw $9, $25, -8 ;load local #2 = b
add $10, $0, 4 ;save 4 in $10

;calculate a*b
mul $8, $8, $9
;calculate (a*b) -4
sub $8, $8, $10
stw $8,$25, -12 ;save local #3 = c

;call printi
ldw $8, $25, -4 ;load value for a
stw $8, $29, 0 ;store as #1 out-arg
jal printi

  ;print whitespace
  add $8, $0, 0x20
  stw $8, $29, 0
  jal printc

ldw $8, $25, -8 ;load value for b
stw $8, $29, 0 ;store as #1 out-arg
jal printi

  ;print whitespace
  add $8, $0, 0x20
  stw $8, $29, 0
  jal printc

ldw $8, $25, -12 ;load value for c
stw $8, $29, 0 ;store as #1 out-arg
jal printi

  ;print line-break
  add $8, $0, 0xA
  stw $8, $29, 0
  jal printc

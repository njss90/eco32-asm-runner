main:
add $8, $0, 0     ;sum
add $9, $0, 0     ;i

startLbl:
    add $10, $0, 10     ;save 10 in $10
    bge $9, $10, end

    ;calc i*2
    mul $10, $9, 2
    ;calc sum + (i*2)
    add $8, $8, $10
    ;increment i
    add $9, $9, 1
    j startLbl
end:

; save all calculated values:
; $8 = sum
; $9 = i
stw $8, $25, -4
stw $9, $25, -8

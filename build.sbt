compileOrder := CompileOrder.Mixed

scalaSource in Compile <<= (baseDirectory in Compile)(_ / "src")

scalaSource in Test <<= (baseDirectory in Test)(_ / "test")

javaSource in Compile <<= (baseDirectory in Compile)(_ / "src")

javaSource in Test <<= (baseDirectory in Test)(_ / "test")

resourceDirectory in Compile <<= (baseDirectory in Compile)(_ / "assets")

resourceDirectory in Test <<= (baseDirectory in Test)(_ / "assets")

classDirectory in Compile <<= (baseDirectory in Compile)(_ / "bin")

classDirectory in Test <<= (baseDirectory in Test)(_ / "bin")

cleanFiles <+= baseDirectory (_ / "bin")

//EclipseKeys.withSource := true
//tell sbteclipse that there's a generated src
EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Managed

//run only if java 1.8 is installed
initialize := {
  val _ = initialize.value // run the previous initialization
  val required = "1.8"
  val current  = sys.props("java.specification.version")
  assert(current == required, s"Unsupported JDK: $current != $required")
}

lazy val genScripts = taskKey[Unit]("Generates scripts to execute the program more easy.")

genScripts := {
	val (_, jarfile) = packagedArtifact.in(Compile, packageBin).value
	
	val scriptsDir = jarfile.getParentFile() / "scripts" 
	val simsh = scriptsDir / "sim.sh"
	val simgsh = scriptsDir / "sim-g.sh"
	val simbat = scriptsDir / "sim.bat"
	val simgbat = scriptsDir / "sim-g.bat"
	
	val jar = jarfile.getName
	val batPrefix = s"@echo off\n\nSET jar=./$jar\n\n"
	val bashPrefix = s"#!/bin/bash\n\njar=./$jar\n\n"
	
	IO.write(simsh, bashPrefix + "scala -cp $jar ecosim.EcoSimulator $*\n")
	IO.write(simgsh, bashPrefix + "scala -cp $jar ecosim.interactive.InteractiveSimulator $*\n")
	
	IO.write(simbat, batPrefix + "scala -cp %jar% ecosim.EcoSimulator %*\n")
	IO.write(simgbat, batPrefix + "scala -cp %jar% ecosim.interactive.InteractiveSimulator %*\n")
}

lazy val customSettings = Seq(
  organization := "de.thm",
  version := "2.0.4",
  scalaVersion := "2.11.7",
  name := "Eco32-Asm-Simulator",
  //check java 1.8 for compiling
  javacOptions ++= Seq("-source", "1.8")
)

lazy val codeInjections = Seq(
  buildInfoPackage := "build.util", //generated pckg
  buildInfoObject := "SbtBuild", //generated object-name
  //generated keys
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
)

lazy val proj = (project in file(".")).settings(customSettings: _*).
  enablePlugins(BuildInfoPlugin).settings(codeInjections:_*)


libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"

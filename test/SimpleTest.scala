/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file license for copying permission.
 */

import org.junit.Assert._
import org.junit.Test

class SimpleTest {
  @Test
  def test() = {
    /*
     * Basically a test-class for testing junit itself.. e.g.: is it working correctly in sbt?.
     * commands for sbt:
     * - show test:definedTestNames => shows all recognized tests
     * - testOnly <Tests*>          => tests only the given tests
     * - test                       => run all tests
     */

    val o = new Object()
    if (o eq null)
      fail()
  }
}

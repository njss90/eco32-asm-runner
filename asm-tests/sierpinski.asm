	.import	printi
	.import	printc
	.import	readi
	.import	readc
	.import	exit
	.import	time
	.import	clearAll
	.import	setPixel
	.import	drawLine
	.import	drawCircle
	.import	_indexError

	.code
	.align	4

	.export	plot
    j main
plot:
	sub	$29,$29,44		; allocate frame
	stw	$25,$29,24		; save old frame pointer
	add	$25,$29,44		; setup new frame pointer
	stw	$31,$25,-24		; save return register
	add	$8,$25,-4
	add	$9,$25,0
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$0,128
	sub	$9,$9,$10
	add	$10,$0,3
	mul	$9,$9,$10
	add	$10,$0,2
	div	$9,$9,$10
	add	$10,$0,320
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-8
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$0,128
	sub	$9,$9,$10
	add	$10,$0,3
	mul	$9,$9,$10
	add	$10,$0,2
	div	$9,$9,$10
	add	$10,$0,240
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-12
	add	$9,$25,8
	ldw	$9,$9,0
	add	$10,$0,128
	sub	$9,$9,$10
	add	$10,$0,3
	mul	$9,$9,$10
	add	$10,$0,2
	div	$9,$9,$10
	add	$10,$0,320
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-16
	add	$9,$25,12
	ldw	$9,$9,0
	add	$10,$0,128
	sub	$9,$9,$10
	add	$10,$0,3
	mul	$9,$9,$10
	add	$10,$0,2
	div	$9,$9,$10
	add	$10,$0,240
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-4
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-12
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-16
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$0,0
	stw	$8,$29,16		; store arg #4
	jal	drawLine
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	stw	$9,$8,0
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,12
	ldw	$9,$9,0
	stw	$9,$8,0
	ldw	$31,$25,-24		; restore return register
	ldw	$25,$29,24		; restore old frame pointer
	add	$29,$29,44		; release frame
	jr	$31			; return

	.export	a
a:
	sub	$29,$29,32		; allocate frame
	stw	$25,$29,28		; save old frame pointer
	add	$25,$29,32		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,0
	ble	$8,$9,L0
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	a
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	b
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$0,2
	add	$11,$25,12
	ldw	$11,$11,0
	mul	$10,$10,$11
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	d
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	a
L0:
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,28		; restore old frame pointer
	add	$29,$29,32		; release frame
	jr	$31			; return

	.export	b
b:
	sub	$29,$29,32		; allocate frame
	stw	$25,$29,28		; save old frame pointer
	add	$25,$29,32		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,0
	ble	$8,$9,L1
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	b
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	c
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$0,2
	add	$11,$25,12
	ldw	$11,$11,0
	mul	$10,$10,$11
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	a
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	b
L1:
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,28		; restore old frame pointer
	add	$29,$29,32		; release frame
	jr	$31			; return

	.export	c
c:
	sub	$29,$29,32		; allocate frame
	stw	$25,$29,28		; save old frame pointer
	add	$25,$29,32		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,0
	ble	$8,$9,L2
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	c
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	d
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$0,2
	add	$11,$25,12
	ldw	$11,$11,0
	mul	$10,$10,$11
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	b
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	c
L2:
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,28		; restore old frame pointer
	add	$29,$29,32		; release frame
	jr	$31			; return

	.export	d
d:
	sub	$29,$29,32		; allocate frame
	stw	$25,$29,28		; save old frame pointer
	add	$25,$29,32		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,0
	ble	$8,$9,L3
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	d
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	a
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$0,2
	add	$11,$25,12
	ldw	$11,$11,0
	mul	$10,$10,$11
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	c
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$25,8
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,8
	ldw	$8,$8,0
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,1
	sub	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	add	$8,$25,8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,16
	ldw	$8,$8,0
	stw	$8,$29,16		; store arg #4
	add	$8,$25,20
	ldw	$8,$8,0
	stw	$8,$29,20		; store arg #5
	jal	d
L3:
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,28		; restore old frame pointer
	add	$29,$29,32		; release frame
	jr	$31			; return

	.export	main
main:
	sub	$29,$29,68		; allocate frame
	stw	$25,$29,28		; save old frame pointer
	add	$25,$29,68		; setup new frame pointer
	stw	$31,$25,-44		; save return register
	add	$8,$25,-36
	add	$9,$0,245
	add	$10,$0,65536
	mul	$9,$9,$10
	add	$10,$0,245
	add	$11,$0,256
	mul	$10,$10,$11
	add	$9,$9,$10
	add	$10,$0,220
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-36
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	jal	clearAll
	add	$8,$25,-16
	add	$9,$0,0
	stw	$9,$8,0
	add	$8,$25,-12
	add	$9,$0,256
	add	$10,$0,4
	div	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-20
	add	$9,$0,2
	add	$10,$25,-12
	ldw	$10,$10,0
	mul	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-24
	add	$9,$0,3
	add	$10,$25,-12
	ldw	$10,$10,0
	mul	$9,$9,$10
	stw	$9,$8,0
L4:
	add	$8,$25,-16
	ldw	$8,$8,0
	add	$9,$0,4
	beq	$8,$9,L5
	add	$8,$25,-16
	add	$9,$25,-16
	ldw	$9,$9,0
	add	$10,$0,1
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-20
	add	$9,$25,-20
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-12
	add	$9,$25,-12
	ldw	$9,$9,0
	add	$10,$0,2
	div	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-24
	add	$9,$25,-24
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-4
	add	$9,$25,-20
	ldw	$9,$9,0
	stw	$9,$8,0
	add	$8,$25,-8
	add	$9,$25,-24
	ldw	$9,$9,0
	stw	$9,$8,0
	add	$8,$25,-28
	add	$9,$25,-4
	ldw	$9,$9,0
	stw	$9,$8,0
	add	$8,$25,-32
	add	$9,$25,-8
	ldw	$9,$9,0
	stw	$9,$8,0
	add	$8,$25,-16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-4
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-8
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,-28
	stw	$8,$29,16		; store arg #4
	add	$8,$25,-32
	stw	$8,$29,20		; store arg #5
	jal	a
	add	$8,$25,-4
	add	$9,$25,-4
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-8
	add	$9,$25,-8
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-28
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-32
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-4
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,-16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-4
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-8
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,-28
	stw	$8,$29,16		; store arg #4
	add	$8,$25,-32
	stw	$8,$29,20		; store arg #5
	jal	b
	add	$8,$25,-4
	add	$9,$25,-4
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-8
	add	$9,$25,-8
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-28
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-32
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-4
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,-16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-4
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-8
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,-28
	stw	$8,$29,16		; store arg #4
	add	$8,$25,-32
	stw	$8,$29,20		; store arg #5
	jal	c
	add	$8,$25,-4
	add	$9,$25,-4
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-8
	add	$9,$25,-8
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-28
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-32
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-4
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	add	$8,$25,-16
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-4
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-8
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-12
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	add	$8,$25,-28
	stw	$8,$29,16		; store arg #4
	add	$8,$25,-32
	stw	$8,$29,20		; store arg #5
	jal	d
	add	$8,$25,-4
	add	$9,$25,-4
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-8
	add	$9,$25,-8
	ldw	$9,$9,0
	add	$10,$25,-12
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,-28
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-32
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-4
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,12		; store arg #3
	jal	plot
	j	L4
L5:
	ldw	$31,$25,-44		; restore return register
	ldw	$25,$29,28		; restore old frame pointer
	add	$29,$29,68		; release frame
	jr	$31			; return

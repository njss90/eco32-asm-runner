.import exit

main: sub $9, $0, 5 
	add $8,$0,5
	add $9,$8,5 ; $9 = 10
    bne $8, $9, test
    add $11,$0,2
    j l1
    sub $10,$8,$11 ;$10 = 3
l1:
    mul $15,$8,$8
    j l3

test:
    add $20,$0,35
    l3:
    jal exit

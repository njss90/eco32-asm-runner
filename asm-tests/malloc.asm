.import exit
.import malloc
.import printi

; var adr: int;
; malloc(adr, CONST);
; printi(adr);

sub $29,$29,4    ; local field 'adr'
sub $29,$29,8    ; 8 bytes for methcall

; arguments malloc
add $8, $29, 8   ; Address of field 'adr'
stw $8, $29, 0   ; 1. arg
add $8, $0, 20    ; CONST
stw $8, $29, 4   ; 2. arg
jal malloc

; arguments printi
ldw $8, $29, 8   ; value of field 'adr'
stw $8, $29, 0   ; 1. arg
jal printi

j exit

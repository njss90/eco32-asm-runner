	.import	printi
	.import	printc
	.import	readi
	.import	readc
	.import	exit
	.import	time
	.import	clearAll
	.import	setPixel
	.import	drawLine
	.import	drawCircle
	.import	_indexError

	.code
	.align	4

	.export	main
main:
	sub	$29,$29,40		; allocate frame
	stw	$25,$29,16		; save old frame pointer
	add	$25,$29,40		; setup new frame pointer
	stw	$31,$25,-28		; save return register
	add	$8,$25,-20
	add	$9,$0,1
	stw	$9,$8,0
	add	$8,$25,-12
	add	$9,$0,640
	stw	$9,$8,0
	add	$8,$25,-16
	add	$9,$0,480
	stw	$9,$8,0
L0:
	add	$8,$25,-20
	ldw	$8,$8,0
	add	$9,$0,0
	beq	$8,$9,L1
	add	$8,$25,-8
	stw	$8,$29,0		; store arg #0
	jal	readc
	add	$8,$25,-8
	ldw	$8,$8,0
	add	$9,$0,113
	bne	$8,$9,L2
	add	$8,$25,-20
	add	$9,$0,0
	stw	$9,$8,0
	j	L3
L2:
	add	$8,$25,-12
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-16
	stw	$8,$29,4		; store arg #1
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,8		; store arg #2
	jal	move
L3:
	add	$8,$0,1052842
	stw	$8,$29,0		; store arg #0
	jal	clearAll
	add	$8,$25,-12
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,-16
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	jal	draw
	j	L0
L1:
    jal exit

	.export	draw
draw:
	sub	$29,$29,24		; allocate frame
	stw	$25,$29,20		; save old frame pointer
	add	$25,$29,24		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,2
	div	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$0,2
	div	$8,$8,$9
	stw	$8,$29,4		; store arg #1
	add	$8,$0,32
	stw	$8,$29,8		; store arg #2
	add	$8,$0,0
	stw	$8,$29,12		; store arg #3
	jal	drawCircle
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,20		; restore old frame pointer
	add	$29,$29,24		; release frame
	jr	$31			; return

	.export	move
move:
	sub	$29,$29,8		; allocate frame
	stw	$25,$29,0		; save old frame pointer
	add	$25,$29,8		; setup new frame pointer
	add	$8,$25,-4
	add	$9,$0,16
	stw	$9,$8,0
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$0,119
	bne	$8,$9,L4
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,-4
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
L4:
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$0,97
	bne	$8,$9,L5
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$25,0
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,-4
	ldw	$10,$10,0
	sub	$9,$9,$10
	stw	$9,$8,0
L5:
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$0,115
	bne	$8,$9,L6
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,4
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,-4
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
L6:
	add	$8,$25,8
	ldw	$8,$8,0
	add	$9,$0,100
	bne	$8,$9,L7
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$25,0
	ldw	$9,$9,0
	ldw	$9,$9,0
	add	$10,$25,-4
	ldw	$10,$10,0
	add	$9,$9,$10
	stw	$9,$8,0
L7:
	add	$8,$25,0
	ldw	$8,$8,0
	ldw	$8,$8,0
	add	$9,$0,16
	bge	$8,$9,L8
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,16
	stw	$9,$8,0
	j	L9
L8:
	add	$8,$25,0
	ldw	$8,$8,0
	ldw	$8,$8,0
	add	$9,$0,640
	add	$10,$0,16
	sub	$9,$9,$10
	blt	$8,$9,L10
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,640
	add	$10,$0,17
	sub	$9,$9,$10
	stw	$9,$8,0
L10:
L9:
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	add	$9,$0,32
	bge	$8,$9,L11
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$0,32
	stw	$9,$8,0
	j	L12
L11:
	add	$8,$25,4
	ldw	$8,$8,0
	ldw	$8,$8,0
	add	$9,$0,480
	add	$10,$0,16
	sub	$9,$9,$10
	blt	$8,$9,L13
	add	$8,$25,0
	ldw	$8,$8,0
	add	$9,$0,480
	add	$10,$0,17
	sub	$9,$9,$10
	stw	$9,$8,0
L13:
L12:
	ldw	$25,$29,0		; restore old frame pointer
	add	$29,$29,8		; release frame
	jr	$31			; return

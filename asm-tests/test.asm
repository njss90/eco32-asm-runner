.import printi
.import printc
.import exit
.import setPixel

.export main

main:
; setPixel(int, int, int)
; x = 1
; y = 2
; c = 9

sub $29,$29,12 ; space for 3 ints

add $8,$0,1
stw $8,$29,0

add $8,$0,2
stw $8,$29,4

add $8,$0,9
stw $8,$29,8

jal setPixel
jal exit

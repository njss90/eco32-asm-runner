.import malloc
.import realloc
.import free

main:
    ; 2 lokale vars
    ; max 2 out
    ; + 2 fp, ret reg
    sub $29, $29, 24
    stw $25, $29, 12
    add $25, $29, 24
    stw $31, $25, -16

    ;call malloc with 1 local and size=4
    add $8, $25, -4 ; get adr of local #1
    stw $8, $29, 0 ;store ptr out #1

    add $8, $0, 4 
    stw $8, $29, 4 ;store size, out #2 
    jal malloc 

   ;call 2. malloc
add $8,$25,-8
stw $8,$29,0 ;store ptr out #1
add $8,$0, 10
stw $8,$29,4 ;store size, out #2
jal malloc

; write a[1] = 10
add $8, $25, -4
ldw $8, $8, 0
add $8, $8, 1
add $9, $0, 10
stb $9, $8, 0

; write b[2] = 20
add $8, $25, -8
ldw $8, $8, 0
add $8, $8, 2
add $9, $0, 20
stb $9,$8,0

; write b[6] = 30
add $8, $25, -8
ldw $8, $8, 0
add $8, $8, 6
add $9, $0, 30
stb $9,$8,0

;realloc
add $8, $25, -4 ; adr ptr = adr[0]
add $9, $0, 20 ;new size = 20
stw $8, $29, 0
stw $9, $29, 4
jal realloc


;free
add $8, $25, -4 ; adr ptr = adr[0]
stw $8, $29, 0
;jal free

;free
add $8, $25, -8 ; adr ptr = adr[0]
stw $8, $29, 0
;jal free


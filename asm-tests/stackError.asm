	.import	printi
	.import	printc
	.import	readi
	.import	readc
	.import	exit
	.import	time
	.import	clearAll
	.import	setPixel
	.import	drawLine
	.import	drawCircle
	.import	_indexError

	.code
	.align	4

    j main
	.export	stackOverlfow
stackOverlfow:
	sub	$29,$29,20		; allocate frame
	stw	$25,$29,12		; save old frame pointer
	add	$25,$29,20		; setup new frame pointer
	stw	$31,$25,-12		; save return register
	add	$8,$25,-4
	add	$9,$0,45
	add	$10,$0,4
	add	$9,$9,$10
	stw	$9,$8,0
	add	$8,$25,0
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	stw	$8,$29,4		; store arg #1
	jal	stackOverlfow
	ldw	$31,$25,-12		; restore return register
	ldw	$25,$29,12		; restore old frame pointer
	add	$29,$29,20		; release frame
	jr	$31			; return

	.export	main
main:
	sub	$29,$29,16		; allocate frame
	stw	$25,$29,12		; save old frame pointer
	add	$25,$29,16		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$0,10
	stw	$8,$29,0		; store arg #0
	add	$8,$0,20
	stw	$8,$29,4		; store arg #1
	jal	stackOverlfow
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,12		; restore old frame pointer
	add	$29,$29,16		; release frame
	jr	$31			; return

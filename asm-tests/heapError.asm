	.import	printi
	.import	printc
	.import	readi
	.import	readc
	.import	exit
	.import	time
	.import	clearAll
	.import	setPixel
	.import	drawLine
	.import	drawCircle
	.import	_indexError
    .import malloc

	.code
	.align	4

    j main
	.export	test
test:
	sub	$29,$29,20		; allocate frame
	stw	$25,$29,12		; save old frame pointer
	add	$25,$29,20		; setup new frame pointer
	stw	$31,$25,-12		; save return register
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$25,0
	ldw	$9,$9,0
	bge	$8,$9,L0
	add	$8,$25,0
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	add	$8,$25,4
	ldw	$8,$8,0
	add	$9,$0,1
	add	$8,$8,$9
	stw	$8,$29,4		; store arg #1
	jal	test
	j	L1
L0:
L2:
    ;allocate some memory
    add $8,$25,-4
    stw $8,$29,0
    add $9,$0,4
    stw $9,$29,4
    jal malloc

    ;loop-stuff
	add	$8,$0,1
	add	$9,$0,1
	bne	$8,$9,L3
	j	L2
L3:
L1:
	ldw	$31,$25,-12		; restore return register
	ldw	$25,$29,12		; restore old frame pointer
	add	$29,$29,20		; release frame
	jr	$31			; return

	.export	main
main:
	sub	$29,$29,16		; allocate frame
	stw	$25,$29,12		; save old frame pointer
	add	$25,$29,16		; setup new frame pointer
	stw	$31,$25,-8		; save return register
	add	$8,$0,20
	stw	$8,$29,0		; store arg #0
	add	$8,$0,0
	stw	$8,$29,4		; store arg #1
	jal	test
	ldw	$31,$25,-8		; restore return register
	ldw	$25,$29,12		; restore old frame pointer
	add	$29,$29,16		; release frame
	jr	$31			; return

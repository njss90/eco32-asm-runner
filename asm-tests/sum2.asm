	.import	printi
	.import	printc
	.import	readi
	.import	readc
	.import	exit
	.import	time
	.import	clearAll
	.import	setPixel
	.import	drawLine
	.import	drawCircle
	.import	_indexError

	.code
	.align	4

	.export	main
main:
	sub	$29,$29,20		; allocate frame
	stw	$25,$29,8		; save old frame pointer
	add	$25,$29,20		; setup new frame pointer
	stw	$31,$25,-16		; save return register
	add	$8,$25,-8
	add	$9,$0,0
	add	$10,$0,2
	bgeu	$9,$10,_indexError
	mul	$9,$9,4
	add	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	jal	readi
	add	$8,$25,-8
	add	$9,$0,1
	add	$10,$0,2
	bgeu	$9,$10,_indexError
	mul	$9,$9,4
	add	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	jal	readi
	add	$8,$25,-8
	add	$9,$0,0
	add	$10,$0,2
	bgeu	$9,$10,_indexError
	mul	$9,$9,4
	add	$8,$8,$9
	ldw	$8,$8,0
	add	$9,$25,-8
	add	$10,$0,1
	add	$11,$0,2
	bgeu	$10,$11,_indexError
	mul	$10,$10,4
	add	$9,$9,$10
	ldw	$9,$9,0
	add	$8,$8,$9
	stw	$8,$29,0		; store arg #0
	jal	printi
	add	$8,$0,10
	stw	$8,$29,0		; store arg #0
	jal	printc
    jal exit

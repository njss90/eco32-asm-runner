.import printc
.import drawCircle
.import exit

.export main

main:
; drawCircle(x: int, y: int, r: int, c: int)
sub $29,$29,16

add $8,$0,320
stw $8,$29,0

add $8,$0,240
stw $8,$29,4

add $8,$0,25
stw $8,$29,8

add $8,$0,0xFF00FF
stw $8,$29,12

jal drawCircle
jal exit

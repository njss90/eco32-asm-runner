	.import	printi
	.import	printc
	.import	readi
	.import	readc
	.import	exit
	.import	time
	.import	clearAll
	.import	setPixel
	.import	drawLine
	.import	drawCircle
	.import	_indexError

	.code
	.align	4

	.export	main
main:
	sub	$29,$29,20		; allocate frame
	stw	$25,$29,8		; save old frame pointer
	add	$25,$29,20		; setup new frame pointer
	stw	$31,$25,-16		; save return register
	add	$8,$25,-4
	stw	$8,$29,0		; store arg #0
	jal	readi
	add	$8,$25,-8
	stw	$8,$29,0		; store arg #0
	jal	readc
	add	$8,$25,-4
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	jal	printi
	add	$8,$25,-8
	ldw	$8,$8,0
	stw	$8,$29,0		; store arg #0
	jal	printc
    jal exit

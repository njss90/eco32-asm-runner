/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

import scala.io.Source
import scala.collection.mutable.ArrayBuffer
import util._
import ecosim.gfx.EcoWindow
import ecosim.instructions.Instr

object EcoSimulator {

  private def printHelpAndExit(): Unit = {
    println("Usage: sim <Options> <Assembler-File>")
    println("Options:")
    println("\t-gfx show the graphic window")
    println("\t-heap <kbsize> size of heap in KiB")
    println("\t-version print version-number and exit")
    sys.exit(-1)
  }

  private def printVersion(): Unit = {
    println(s"${Constants.programName} version ${Constants.version}")
    println(Constants.copyrightNotice)
  }

  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      printHelpAndExit()
    } else {
      var frame: Option[EcoWindow] = None
      var path = ""
      var heap = 512

      var i = 0
      while (i < args.length) {
        if (args(i) == "-gfx") {
          frame = Some(new EcoWindow())
        } else if (args(i) == "-heap" && args(i + 1).matches("[0-9]+")) {
          heap = args(i + 1).toInt
          i += 1
        } else if(args(i) == "-version") {
          printVersion()
          sys.exit(0)
        } else {
          path = args(i)
        }
        i += 1
      }

      if (path == "") printHelpAndExit()

      val lines = loadAndNormalizeLines(path)
      val (labels, importedLabels) = allLabels(lines)
      val instructions = Instr.parseAll(importedLabels, labels, lines)

      val env = Environment(heap, frame, instructions)
      frame foreach (_.env = env)
      frame foreach (_.setVisible(true))

      (new EcoRuntime(env)).run()

      if (frame.isEmpty) {
        env.exit()
      }
    }
  }
}

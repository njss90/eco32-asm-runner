/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

import scala.util.Try
import scala.util.Success
import scala.util.Failure
import ecosim.util._
import ecosim.gfx.EcoWindow
import ecosim.instructions.Instr

object EcoTestInterface {

  def runProgram(program: Seq[String], heapSizeInKb: Int, enableGfx: Boolean = false): Try[Environment] = {
    var frame: Option[EcoWindow] = if (enableGfx) Some(new EcoWindow()) else None

    val lines = normalizeLines(program.mkString("\n"))
    val (labels, importedLabels) = allLabels(lines)
    val instructions = Instr.parseAll(importedLabels, labels, lines)

    val env = Environment(heapSizeInKb, frame, instructions)
    frame foreach (_.env = env)
    frame foreach (_.setVisible(true))

    val runTry = Try { (new EcoRuntime(env)).run() }

    if (frame.isEmpty) {
      env.exit()
    }

    runTry.map(_ => env)
  }

}
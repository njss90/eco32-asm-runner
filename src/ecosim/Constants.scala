/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

object Constants {

  private val editorNameNoVersion = "Eco-Assembler Editor & Debugger"
  lazy val editorName = editorNameNoVersion + " - " + version
  lazy val editorPreferences = editorNameNoVersion + " - Preferences" + " - " + version
  lazy val graphicWindow = "Eco-Simulator Graphics Window" + " - " + version

  lazy val programName = build.util.SbtBuild.name
  lazy val version = build.util.SbtBuild.version
  lazy val copyrightYear = 2015
  lazy val copyrightNotice = s"Coppyright $copyrightYear N. Justus, J. Sladek, MIT License"
}

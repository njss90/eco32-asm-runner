/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

import scala.io.Source
import scala.collection.mutable.ArrayBuffer
import java.io.File
import java.io.IOException
import java.io.FileOutputStream
import java.io.PrintWriter
import java.io.FileInputStream
import java.util.Scanner

package object util {

  def runnable(task: => Unit) = new Runnable() {
    override def run(): Unit = task
  }

  def asset(name: String) =
    getClass.getResourceAsStream("/assets/" + name)

  val killProgrammOnError = { msg: String =>
    println(msg)
    sys.exit(-1)
  }

  var error = killProgrammOnError

  def allLabels(lines: Array[String]): (Map[String, Int], Seq[String]) = {
    val label = "([a-zA-Z_][a-zA-Z0-9_]*):(.*)".r
    val importStm = "\\.import\\s+([a-zA-Z_][a-zA-Z0-9_]*)".r

    val imported = ArrayBuffer[String]()
    val labels = collection.mutable.Map[String, Int]()

    var i = 0
    val iter = lines.iterator
    while (iter.hasNext) {
      val line = iter.next()

      line match {
        case importStm(name) => imported += name
        case label(name, _) =>
          if (labels contains name) {
            val j = labels(name)
            error(s"duplicate definition of label '$name' in line $i and $j !")
          } else {
            labels += name -> i
          }
        case _ =>
      }

      i += 1
    }

    (labels.toMap, imported.distinct.sorted)
  }

  def getText(path: String): String =
    Source.fromFile(path).mkString

  private lazy val trimAndKillComment: String => String = 
    ((_: String).trim()) andThen killComment
    
  def normalizeLines(text: String) =
    (text split "\n") map trimAndKillComment
    
  def loadAndNormalizeLines(path: String) = 
    (Source.fromFile(path).getLines() map trimAndKillComment).toArray

  private def killComment(line: String) = (line indexOf ";") match {
    case i if (i < 0) => line
    case i            => line substring (0, i)
  }

  def putText(file: File, text: String) = try {
    val stream = new FileOutputStream(file)
    val printer = new PrintWriter(stream)
    printer.println(text)
    printer.flush()
    printer.close()
  } catch {
    case e: IOException =>
  }

  def findMain(map: Map[String, Int]) =
    map.get("main").getOrElse {
      error("no main label defined!")
    }

  /** creates an unstarted thread */
  def thread(task: => Unit) = new Thread(runnable(task))

}

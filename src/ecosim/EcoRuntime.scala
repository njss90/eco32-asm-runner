/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

import util._
import Environment._
import ecosim.lib.StdLib
import ecosim.instructions.InstructionExtractors

class EcoRuntime(
  private val env: Environment) {

  import env._

  def run(): Unit = {
    ip -= 1
    while (ip < instructions.length && running) {
      ip += 1
      if (ip < instructions.length) {
        val instruction = instructions(ip)
        if (instruction ne null) {
          instruction.run(env)
        }
      }
    }
  }

  private def printRegisters() = {
    println("Registers:")
    for (i <- 0 until 32) {
      if (isBadRegister(i))
        println("$" + i + ": DO NOT USE!")
      else println("$" + i + ": " + readRegister(i))
    }
  }
}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.lib

import ecosim.Environment
import ecosim.lib.CallHelper._
import ecosim.util._
import ecosim.gfx._

import scala.io.StdIn

private[lib] object LibFunctions {

  private def testGfx(env: Environment)(f: EcoWindow => Unit): Unit =
    f(env.frame.getOrElse(error("the graphics window is not enabled")))

  // printi(n: int)
  def printi(env: Environment) = call(env) {
    import env._
    print(getArg(0))
    System.out.flush()
  }

  // printc(c: int)
  def printc(env: Environment) = call(env) {
    import env._
    print(getArg(0).toChar)
    System.out.flush()
  }

  // readi(ref i: int)
  def readi(env: Environment) = call(env) {
    import env._
    val adr = getArg(0)
    val ramidx = addressToIndex(adr)
    val v = readInt()
    ram.setInt(ramidx, v)
  }

  private lazy val number = "-?[0-9]+".r
  private def readInt(): Int = {
    var line = ""
    do {
      line = scanner.nextLine()
    } while (number.findFirstIn(line).isEmpty)
    line.toInt
  }

  // readc(ref i: int)
  def readc(env: Environment) = call(env) {
    import env._
    val adr = getArg(0)
    val ramidx = addressToIndex(adr)
    val v = readChar()
    ram.setInt(ramidx, v)
  }

  private def readChar(): Char = {
    var line = ""
    do {
      line = scanner.nextLine().trim()
    } while (line.isEmpty())
    line charAt 0
  }

  private lazy val scanner = new java.util.Scanner(System.in)

  // time(ref i: int)
  def time(env: Environment) = call(env) {
    import env._
    val adr = getArg(0)
    val ramidx = addressToIndex(adr)
    ram.setInt(ramidx, timeRunning())
  }

  // setPixel(x: int, y: int, color: int)
  def setPixel(env: Environment) = call(env) {
    import env._
    testGfx(env) { window =>
      val x = getArg(0)
      val y = getArg(1)
      val c = getArg(2)
      window.draw(DrawPixel(x, y, c))
    }
  }

  // clearAll(color: int)
  def clearAll(env: Environment) = call(env) {
    import env._
    testGfx(env) { window =>
      val c = getArg(0)
      window.draw(Fill(c))
    }
  }

  // drawLine(x1: int, y1: int, x2: int, y2: int, color: int)
  def drawLine(env: Environment) = call(env) {
    import env._
    testGfx(env) { window =>
      val x1 = getArg(0)
      val y1 = getArg(1)
      val x2 = getArg(2)
      val y2 = getArg(3)
      val c = getArg(4)
      window.draw(DrawLine(x1, y1, x2, y2, c))
    }
  }

  // drawCircle(x0: int, y0: int, radius: int, color: int)
  def drawCircle(env: Environment) = call(env) {
    import env._
    testGfx(env) { window =>
      val x = getArg(0)
      val y = getArg(1)
      val r = getArg(2)
      val c = getArg(3)
      window.draw(DrawCircle(x, y, r, c))
    }
  }

  def indexOutOfBoundsException(env: Environment) = {
    error(s"Index out of range in instruction ${env.ip}!")
  }

  def exit(env: Environment) = env.running = false
}

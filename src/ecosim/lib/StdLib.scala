/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.lib

import ecosim.Environment

object StdLib {
  import LibFunctions._
  import Heapmanager._

  private val lib: Map[String, (Environment) => Unit] = Map(
    "printi" -> printi,
    "printc" -> printc,
    "readi" -> readi,
    "readc" -> readc,
    "time" -> time,

    "clearAll" -> clearAll,
    "setPixel" -> setPixel,
    "drawLine" -> drawLine,
    "drawCircle" -> drawCircle,

    "malloc" -> malloc,
    "free" -> free,
    "realloc" -> realloc,

    "_indexError" -> indexOutOfBoundsException,
    "exit" -> exit)

  def apply(name: String) = lib.get(name).get
  def exists(name: String) = lib.get(name).isDefined
}

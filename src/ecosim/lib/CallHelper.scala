/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.lib

import ecosim.Environment

private[lib] object CallHelper {

  private[lib] def call(env: Environment, localsize: Int = 0)(fn: => Unit) = {
    env.saveRegisters(localsize)
    fn
    env.restoreRegisters(localsize)
    env.doReturn()
  }

}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.lib

import ecosim.Environment
import ecosim.lib.CallHelper._
import ecosim.util._

object Heapmanager {

  private val magicnr = 0x77A5844C

  // malloc(ref ret: int, size: int)
  def malloc(env: Environment) = call(env) { malloc_impl(env) }

  private def malloc_impl(env: Environment) = {
    import env._

    val adr = getArg(0)
    val size = getArg(1)
    var i = 0

    def skipNonFree() =
      while (ram.getInt(i) == magicnr) {
        i += 4 // skip magicnr
        val size = ram.getInt(i)
        i += 4 // skip size
        i += size // skip data block
      }

    def skipFree(): Boolean = {
      var j = i
      while (j < size - 4) {
        if (ram.getInt(j) == magicnr) {
          i = j
          return true
        }
        j += 1
      }
      false
    }

    do {
      skipNonFree()
    } while (skipFree())

    setHeapIdx(i + 8 + size)

    ram.setInt(i, magicnr)
    ram.setInt(i + 4, size)

    val ramidx = addressToIndex(adr)
    ram.setInt(ramidx, i + 8)
  }

  // realloc(ref adr: int, newsize: int)
  def realloc(env: Environment) = call(env) {
    import env._
    val oldAdr = ram.getInt(addressToIndex(getArg(0)))
    val oldSize = oldAdr - 4
    val newSize = env.getArg(1)

    if (oldSize != newSize) {
      malloc_impl(env)
      val newAdr = ram.getInt(addressToIndex(addressToIndex(env.getArg(0))))
      for (idx <- 0 until oldSize by 4) {
        val ramIdx2 = env.addressToIndex(newAdr + idx)
        val v = env.ram.getInt(env.addressToIndex(oldAdr + idx))
        env.ram.setInt(ramIdx2, v)
      }
    }
  }

  // free(adr:Int)
  def free(env: Environment) = call(env) {
    import env._
    val ramidx = addressToIndex(getArg(0))
    val adrMagicNo = ram.getInt(ramidx) - 8

    if (ram.getInt(adrMagicNo) != magicnr)
      error("Segmentation fault.")

    ram.setInt(adrMagicNo, 0)
    val adrSize = adrMagicNo + 4
    ram.setInt(adrSize, 0)
  }
}

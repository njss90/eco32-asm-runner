/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ecosim.Environment

trait LdInstr extends Instr

case class LdwInstr(
  r1: R, r2: R, n: Int) extends LdInstr {

  def run(env: Environment): Unit = {
    import env._
    val ramidx = addressToIndex(readRegister(r2.n) + n)
    writeRegister(r1.n, ram.getInt(ramidx))
  }

}

case class LdbInstr(
  r1: R, r2: R, n: Int) extends LdInstr {

  def run(env: Environment): Unit = {
    import env._
    val ramidx = addressToIndex(readRegister(r2.n) + n)
    writeRegister(r1.n, ram.getByte(ramidx))
  }

}

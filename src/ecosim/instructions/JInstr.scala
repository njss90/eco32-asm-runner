/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ecosim.Environment

sealed trait JInstr extends Instr

case class JLineInstr(
  line: Int) extends JInstr {

  def run(env: Environment): Unit = {
    import env._
    ip = line
  }

}

case class JStdInstr(
  func: Environment => Unit) extends JInstr {

  def run(env: Environment): Unit = {
    import env._
    func(env)
  }

}

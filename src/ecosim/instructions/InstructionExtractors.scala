/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ArithType._
import JumpType._

trait InstructionExtractors {

  trait EcoExtractor[A] {
    def unapply(line: String): Option[A]
  }

  private val register = "\\$([0-9]+)"
  private val number = "(-?[0-9]+)"
  private val hexnumber = "0x([0-9a-fA-F]+)"
  private val comma = "\\s*,\\s*"
  private val labelRegex = "[a-zA-Z_]|[a-zA-Z_][a-zA-Z0-9_]+"

  val asmDirectives = new EcoExtractor[String] {
    private val regex = ("\\s*" + "\\.(.*)").r

    def unapply(line: String) = line match {
      case regex(direc) => Some(direc)
      case _            => None
    }
  }

  val label = new EcoExtractor[(String, Option[String])] {
    // matches label:
    private val regex1 = ("\\s*" + "(" + labelRegex + "):\\s*").r
    // matches label: add $4,$8,0
    private val regex2 = ("\\s*" + "(" + labelRegex + "):(.*)").r

    def unapply(line: String) = line match {
      case s @ regex1(labelName) =>
        Some(labelName, None)
      case s @ regex2(labelName, instr) =>
        Some(labelName, Some(instr.trim()))
      case _ => None
    }
  }

  val j = jumpLabel("j")
  val jal = jumpLabel("jal")
  val jr = jumpReg("jr")

  /*
   * ============== MEMORY MANAGEMENT
   */
  /** load word */
  val ldw = memoryExtractor("ldw")
  /** load byte */
  val ldb = memoryExtractor("ldb")
  /** store word */
  val stw = memoryExtractor("stw")
  /** store byte */
  val stb = memoryExtractor("stb")

  private def memoryExtractor(t: String) = new EcoExtractor[(R, R, Int)] {
    // stw $15,$20,4
    val regex1 = ("\\s*" + t + "\\s+" + register + comma + register + comma + number + "\\s*").r

    // stw $15,$20,0xaffe
    val regex2 = ("\\s*" + t + "\\s+" + register + comma + register + comma + hexnumber + "\\s*").r

    def unapply(line: String): Option[(R, R, Int)] = line match {
      case regex1(a, b, c) => Some(R(a.toInt), R(b.toInt), c.toInt)
      case regex1(a, b, c) => Some(R(a.toInt), R(b.toInt), Integer.parseInt(c, 16))
      case _               => None
    }
  }

  val arithAndLogicExtractor = new EcoExtractor[(ArithType.ArithOp, R, R, Either[R, Int])] {
    // add $4, $5,  0
    private val regex1 = ("\\s*" + "([a-zA-Z]{2,4})" + "\\s+" + register + comma + register + comma + number + "\\s*").r
    // add $4, $5,  0
    private val regex2 = ("\\s*" + "([a-zA-Z]{2,4})" + "\\s+" + register + comma + register + comma + hexnumber + "\\s*").r
    // add $4, $5, $0
    private val regex3 = ("\\s*" + "([a-zA-Z]{2,4})" + "\\s+" + register + comma + register + comma + register + "\\s*").r

    def unapply(line: String): Option[(ArithType.ArithOp, R, R, Either[R, Int])] = line match {
      case regex1(tystr, a, b, c) =>
        getOperation(tystr) map { ty => (ty, R(a.toInt), R(b.toInt), Right(c.toInt)) }
      case regex2(tystr, a, b, c) =>
        getOperation(tystr) map { ty => (ty, R(a.toInt), R(b.toInt), Right(Integer.parseInt(c, 16))) }
      case regex3(tystr, a, b, c) =>
        getOperation(tystr) map { ty => (ty, R(a.toInt), R(b.toInt), Left(R(c.toInt))) }
      case _ => None
    }

    @inline
    private def getOperation(op: String) =
      op.toLowerCase() match {
        case "add"  => Some(ADD)
        case "sub"  => Some(SUB)
        case "mul"  => Some(MUL)
        case "div"  => Some(DIV)
        case "rem"  => Some(REM)
        case "and"  => Some(AND)
        case "or"   => Some(OR)
        case "xor"  => Some(XOR)
        case "xnor" => Some(XNOR)
        case "sll"  => Some(SLL)
        case "slr"  => Some(SLR)
        case "sar"  => Some(SAR)
        case _      => None
      }
  }

  val conditionJump = new EcoExtractor[(JumpType.JumpOp, R, R, String)] {
    private val regex = ("\\s*" + "([a-zA-Z]{3,4})" + "\\s+" + register + comma + register + comma + "(" + labelRegex + ")" + "\\s*").r

    def unapply(line: String): Option[(JumpType.JumpOp, R, R, String)] = line match {
      case regex(tystr, a, b, lbl) => getOperation(tystr) map { ty => (ty, R(a.toInt), R(b.toInt), lbl) }
      case _                       => None
    }

    @inline
    private def getOperation(op: String) = op.toLowerCase() match {
      case "beq"  => Some(BEQ)
      case "bne"  => Some(BNE)
      case "ble"  => Some(BLE)
      case "blt"  => Some(BLT)
      case "bge"  => Some(BGE)
      case "bgt"  => Some(BGT)
      case "bgeu" => Some(BGEU)
      case _      => None
    }
  }

  private def jumpReg(t: String) = new EcoExtractor[R] {
    //jr $5
    private val regex = ("\\s*" + t + "\\s+" + register + "\\s*").r

    def unapply(line: String): Option[R] = line match {
      case regex(a) => Some(R(a.toInt))
      case _        => None
    }
  }

  private def jumpLabel(t: String) = new EcoExtractor[String] {
    //j TESTLABEL
    private val regex = ("\\s*" + t + "\\s+(" + labelRegex + ")\\s*").r

    def unapply(line: String): Option[String] = line match {
      case regex(a) => Some(a)
      case _        => None
    }
  }

}

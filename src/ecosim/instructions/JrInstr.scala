/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ecosim.Environment
import Environment._

case class JrInstr(
  r: R) extends Instr {

  def run(env: Environment): Unit = {
    import env._
    val no = readRegister(r.n)
    // exit program because end of main
    ip = if (no == stopExec) instructions.length else no
  }

}

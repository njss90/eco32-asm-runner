/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import JumpType._
import ecosim.Environment
import ecosim.lib.StdLib

sealed trait CondJumpInstr extends Instr

case class CondJumpLineInstr(
  op: JumpOp,
  reg1: R, reg2: R,
  line: Int) extends CondJumpInstr {

  def run(env: Environment): Unit = {
    import env._
    val reg1Val = readRegister(reg1.n)
    val reg2Val = readRegister(reg2.n)

    if (op(reg1Val, reg2Val)) {
      ip = line
    }
  }

}

case class CondJumpStdInstr(
  op: JumpOp,
  reg1: R, reg2: R,
  func: Environment => Unit) extends CondJumpInstr {

  def run(env: Environment): Unit = {
    import env._
    val reg1Val = readRegister(reg1.n)
    val reg2Val = readRegister(reg2.n)

    if (op(reg1Val, reg2Val)) {
      func(env)
    }
  }

}

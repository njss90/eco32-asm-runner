/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

object ArithType extends Enumeration {
  case class ArithOp(op: (Int, Int) => Int) extends super.Val {
    def apply(a: Int, b: Int) = op(a, b)
  }

  val ADD = ArithOp { case (a, b) => a + b }
  val SUB = ArithOp { case (a, b) => a - b }
  val MUL = ArithOp { case (a, b) => a * b }
  val DIV = ArithOp { case (a, b) => a / b }
  val REM = ArithOp { case (a, b) => a % b }
  val AND = ArithOp { case (a, b) => a & b }
  val OR = ArithOp { case (a, b) => a | b }
  val XOR = ArithOp { case (a, b) => a ^ b }
  val XNOR = ArithOp { case (a, b) => ~(a ^ b) }
  val SLL = ArithOp { case (a, b) => a << b }
  val SLR = ArithOp { case (a, b) => a >> b }
  val SAR = ArithOp { case (a, b) => a >>> b }
}

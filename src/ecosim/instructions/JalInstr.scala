/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ecosim.Environment

sealed trait JalInstr extends Instr

case class JalLineInstr(
  line: Int) extends JalInstr {

  def run(env: Environment): Unit = {
    import env._
    writeRegister(31, ip)
    ip = line
  }

}

case class JalStdInstr(
  func: Environment => Unit) extends JalInstr {

  def run(env: Environment): Unit = {
    import env._
    writeRegister(31, ip)
    func(env)
  }

}

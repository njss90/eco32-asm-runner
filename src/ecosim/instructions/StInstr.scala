/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ecosim.Environment

trait StInstr extends Instr

case class StwInstr(
  r1: R, r2: R, n: Int) extends StInstr {

  def run(env: Environment): Unit = {
    import env._
    val ramidx = addressToIndex(readRegister(r2.n) + n)
    ram.setInt(ramidx, readRegister(r1.n))
  }

}

case class StbInstr(
  r1: R, r2: R, n: Int) extends StInstr {

  def run(env: Environment): Unit = {
    import env._
    val ramidx = addressToIndex(readRegister(r2.n) + n)
    ram.setByte(ramidx, (readRegister(r1.n) & 0xFF).toByte)
  }

}

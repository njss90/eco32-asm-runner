/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

object JumpType extends Enumeration {
  case class JumpOp(val op: (Int, Int) => Boolean) extends super.Val {
    def apply(a: Int, b: Int) = op(a, b)
  }

  val BEQ = JumpOp { case (x, y) => x == y }
  val BNE = JumpOp { case (x, y) => x != y }
  val BLE = JumpOp { case (x, y) => x <= y }
  val BLT = JumpOp { case (x, y) => x < y }
  val BGE = JumpOp { case (x, y) => x >= y }
  val BGT = JumpOp { case (x, y) => x > y }
  val BGEU = JumpOp {
    case (x, y) =>
      // see: http://www.nayuki.io/page/unsigned-int-considered-harmful-for-java
      val xunsigned: Long = x & 0xFFFFFFFFL
      val yunsigned: Long = y & 0xFFFFFFFFL
      xunsigned >= yunsigned
  }
}

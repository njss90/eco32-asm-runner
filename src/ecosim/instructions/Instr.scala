/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ecosim.Environment
import ecosim.lib.StdLib
import ecosim.util._
import scala.collection.mutable.ArrayBuffer

object Instr extends InstructionExtractors {

  def parseAll(importedLabels: Seq[String], localLabels: Map[String, Int], lines: Array[String]) = {
    val buffer = ArrayBuffer[Instr]()

    var i = 0
    while (i < lines.length) {
      buffer += parse(importedLabels, localLabels, lines(i), i)
      i += 1
    }

    buffer.toArray
  }

  private def parse(importedLabels: Seq[String], localLabels: Map[String, Int], line: String, lineno: Int): Instr =
    line match {

      case arithAndLogicExtractor(op, a, b, c) =>
        c match {
          case Left(r)  => ArithRegInstr(op, a, b, r)
          case Right(n) => ArithNumInstr(op, a, b, n)
        }

      case conditionJump(op, a, b, lbl) =>
        if (importedLabels.contains(lbl) && StdLib.exists(lbl)) {
          CondJumpStdInstr(op, a, b, StdLib(lbl))
        } else {
          if (!localLabels.contains(lbl)) {
            error(s"Label '$lbl' doesn't exist in line $lineno!")
          } else {
            CondJumpLineInstr(op, a, b, localLabels(lbl))
          }
        }

      case j(lbl) =>
        if (importedLabels.contains(lbl) && StdLib.exists(lbl)) {
          JStdInstr(StdLib(lbl))
        } else {
          if (!localLabels.contains(lbl)) {
            error(s"Label '$lbl' doesn't exist in line $lineno!")
          } else {
            JLineInstr(localLabels(lbl))
          }
        }

      case jal(lbl) =>
        if (importedLabels.contains(lbl) && StdLib.exists(lbl)) {
          JalStdInstr(StdLib(lbl))
        } else {
          if (!localLabels.contains(lbl)) {
            error(s"Label '$lbl' doesn't exist in line $lineno!")
          } else {
            JalLineInstr(localLabels(lbl))
          }
        }

      case jr(r) => JrInstr(r)

      case label(labelName, opt) =>
        opt match {
          case None        => null
          case Some(instr) => parse(importedLabels, localLabels, instr, lineno)
        }

      case ldw(r1, r2, n) => LdwInstr(r1, r2, n)
      case ldb(r1, r2, n) => LdbInstr(r1, r2, n)

      case stw(r1, r2, n) => StwInstr(r1, r2, n)
      case stb(r1, r2, n) => StbInstr(r1, r2, n)

      case asmDirectives(_) =>
        null

      case s: String if (s.isEmpty) =>
        null

      case _ =>
        println("Line: " + line)
        error(s"Unknown or false instruction found in line: $lineno!")
    }

}

trait Instr {
  def run(env: Environment): Unit
}

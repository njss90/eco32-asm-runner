/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.instructions

import ArithType._
import ecosim.Environment

sealed trait ArithInstr extends Instr

case class ArithRegInstr(
  op: ArithOp,
  dst: R, reg1: R, reg2: R) extends ArithInstr {

  def run(env: Environment): Unit = {
    import env._
    val reg1Val = readRegister(reg1.n)
    val reg2Val = readRegister(reg2.n)
    writeRegister(dst.n, op(reg1Val, reg2Val))
  }

}

case class ArithNumInstr(
  op: ArithOp,
  dst: R, reg1: R, n: Int) extends Instr {

  def run(env: Environment): Unit = {
    import env._
    val reg1Val = readRegister(reg1.n)
    writeRegister(dst.n, op(reg1Val, n))
  }

}

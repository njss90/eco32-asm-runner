/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.gfx

import javax.swing.{ WindowConstants, JFrame, JComponent, SwingUtilities }
import java.awt.Dimension
import java.awt.Graphics
import java.awt.image.BufferedImage
import ecosim.util._
import java.awt.event.WindowEvent
import java.awt.event.WindowAdapter
import ecosim.Environment
import ecosim.Constants._

class EcoWindow extends JComponent {

  private val frame = new JFrame(graphicWindow)
  frame.getContentPane.setPreferredSize(new Dimension(640, 480))
  frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)
  frame.setResizable(false)
  frame.pack()
  frame.add(this)

  var env: Environment = null

  frame.addWindowListener(new WindowAdapter() {
    override def windowClosing(e: WindowEvent): Unit = {
      env.exit()
    }
  })

  private var running = true

  private val fps = 1000 / 30
  private val updater = thread {
    while (running) {
      SwingUtilities.invokeLater(runnable {
        repaint()
      })
      try Thread.sleep(fps)
      catch { case e: Exception => }
    }
  }
  updater.start()

  private val image = new BufferedImage(640, 480, BufferedImage.TYPE_INT_RGB)
  private val g = image.createGraphics()

  override final def paint(e: Graphics): Unit =
    e.drawImage(image, 0, 0, null)

  def draw(draw: Draw) = draw match {
    case d @ DrawPixel(x, y, _) =>
      g.setColor(d.color)
      g.fillRect(x, y, 1, 1)
    case d @ DrawCircle(x, y, r, _) =>
      g.setColor(d.color)
      g.drawOval(x - r / 2, y - r / 2, r, r)
    case d @ DrawLine(x1, y1, x2, y2, _) =>
      g.setColor(d.color)
      g.drawLine(x1, y1, x2, y2)
    case d @ Fill(_) =>
      g.setColor(d.color)
      g.fillRect(0, 0, 640, 480)
  }

  override def setVisible(vis: Boolean) = {
    super.setVisible(vis)
    frame.setVisible(vis)
  }

  def exit() = {
    frame.setVisible(false)
    frame.dispose()
    running = false
  }

}

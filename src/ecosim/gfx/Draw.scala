/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.gfx

import java.awt.Color

sealed trait Draw {
  def c: Int
  final def color: Color = new Color(
    (c >>> 16) & 0xFF, (c >>> 8) & 0xFF, c & 0xFF)
}

case class DrawPixel(x: Int, y: Int, c: Int) extends Draw

case class DrawCircle(x: Int, y: Int, r: Int, c: Int) extends Draw

case class DrawLine(x1: Int, y1: Int, x2: Int, y2: Int, c: Int) extends Draw

case class Fill(c: Int) extends Draw

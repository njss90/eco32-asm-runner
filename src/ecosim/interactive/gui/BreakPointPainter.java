/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.BitSet;

import javax.swing.text.JTextComponent;

public class BreakPointPainter extends TextLineNumber implements MouseListener {

	private static final long serialVersionUID = 7132971700995124656L;
	private BitSet breakPoints;
	private int fontSize = -1;
	private Color color;

	public BreakPointPainter(JTextComponent component, Color color) {
		this(component);
		this.color = color;
	}

	public BreakPointPainter(JTextComponent component) {
		super(component);
		breakPoints = new BitSet();
		addMouseListener(this);
	}

	public BreakPointPainter(JTextComponent component, int minimumDisplayDigits) {
		super(component, minimumDisplayDigits);
		breakPoints = new BitSet();
		addMouseListener(this);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		fontSize = getComponent().getFontMetrics(getComponent().getFont()).getHeight();
		if (color != null) {
			g.setColor(color);
		} else {
			g.setColor(Color.DARK_GRAY);
		}
		final FontMetrics metrics = g.getFontMetrics();
		for (int i = 0; i < breakPoints.length(); i++) {
			if (breakPoints.get(i)) {
				int x = 5;
				int y = fontSize * i;
				g.fillOval(x, metrics.getAscent() / 2 + 2 + y, fontSize >> 1, fontSize >> 1);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() != 2) return;
		Integer i = Integer.valueOf(e.getY() / (fontSize = getComponent().getFontMetrics(getComponent().getFont()).getHeight()));
		breakPoints.set(i, !breakPoints.get(i));
		repaint();
	}

	public int[] getBreakPoints() {
		int[] idxs = new int[breakPoints.length()];
		int count = 0;
		for (int i = 0; i < breakPoints.length(); i++) {
			if (breakPoints.get(i)) {
				idxs[count++] = i;
			}
		}
		return Arrays.copyOf(idxs, count);
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive.gui

import java.awt.event.KeyListener
import javax.swing.JComponent
import java.awt.event.KeyEvent
import javax.swing.JTextPane
import java.io.ByteArrayOutputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.io.PrintStream
import javax.swing.JScrollPane
import java.awt.BorderLayout
import javax.swing.ScrollPaneConstants._
import javax.swing.text.BadLocationException
import javax.swing.text.SimpleAttributeSet
import java.io.InputStream
import java.io.IOException

class ConsoleComponent
  extends JComponent
  with KeyListener {

  val console = new JTextPane()
  console.addKeyListener(this)
  val out = new ByteArrayOutputStream()

  val conIn = new ConsoleIn()
  val conOut = new PrintStream(out, true, "UTF-8")

  setLayout(new BorderLayout())
  console.setBorder(null)
  val scroller = new JScrollPane(console, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_ALWAYS)
  scroller.setBorder(null)
  add(scroller, BorderLayout.CENTER)
  new Thread(new OutputWriter(), "OutputWriter").start()

  override def keyTyped(e: KeyEvent) =
    if (e.getKeyChar() == '\n') {
      conIn.enter()
    } else {
      conIn.append(e.getKeyChar())
    }

  def getIn() = conIn
  def getOut() = conOut

  def clearBuffers() = conIn.clearBuffer()

  override def keyPressed(e: KeyEvent) = {}

  override def keyReleased(e: KeyEvent) = {}

  class OutputWriter extends Runnable {
    override def run() = {
      while (!Thread.interrupted()) {
        try {
          Thread.sleep(10);
        } catch { case e: Exception => }
        val data = out.toByteArray();
        if (data.nonEmpty) {
          out.reset();
          try {
            console.getDocument().insertString(console.getDocument().getLength(), new String(data, Charset.forName("UTF-8")), new SimpleAttributeSet());
            console.setCaretPosition(console.getDocument().getLength());
          } catch { case e: BadLocationException => }
        }
      }
    }
  }

  class ConsoleIn extends InputStream {
    private val sb = new java.lang.StringBuilder()
    private val buffer = new java.lang.StringBuilder()
    private lazy val lineSeparator = System.getProperty("line.separator")

    def append(c: Char): Unit = buffer.append(c)

    def enter(): Unit = {
      sb.append(buffer.toString())
      sb.append(System.lineSeparator())
      sb.append(0.toChar)
      buffer.setLength(0)
      this.synchronized { notify() }
    }

    def clearBuffer() = {
      buffer.setLength(0)
      sb.setLength(0)
    }

    override def read(): Int = {
      if (sb.length() == 0) {
        this.synchronized {
          try wait()
          catch { case e: InterruptedException => }
        }
      }
      if (sb.length() == 0) throw new IOException()
      val res = sb.charAt(0).toInt
      sb.deleteCharAt(0)

      if (res == 0) -1
      else res
    }
  }

}

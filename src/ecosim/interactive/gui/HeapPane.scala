/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive.gui

import ecosim.interactive.EcoModel
import javax.swing._
import event._
import javax.swing.JTable
import java.awt._
import javax.swing.table._
import ecosim.util._

class HeapPane(model: EcoModel)
  extends JPanel {

  private val byteSize = model.kbSize << 10
  private val cols = 16
  private val rows = Math.ceil(byteSize.toFloat / 16f).toInt

  private val table = new JTable(new AbstractTableModel() {
    private val names = 0 until 16 map { n => f" $n%01X" }

    override def getColumnName(col: Int): String = names(col)
    override def getRowCount() = rows
    override def getColumnCount() = cols
    override def isCellEditable(row: Int, col: Int) = false

    override def getValueAt(row: Int, col: Int) =
      if (model.env eq null) "00"
      else f"${model.env.ram.getByte(row * cols + col)}%02X"
  })
  table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION)
  table.setRowSelectionAllowed(true)
  table.setColumnSelectionAllowed(true)
  table.setCellSelectionEnabled(true)

  private val rowModel = new DefaultTableModel(rows, 1) {
    override def isCellEditable(row: Int, col: Int) = false
    override def getColumnClass(col: Int) = col match {
      case 0 => classOf[String]
      case _ => super.getColumnClass(col)
    }
  }

  for (row <- 0 until rows) {
    val hexrow = row * cols
    rowModel.setValueAt(f"0x$hexrow%07X", row, 0)
  }
  val headerTable = new JTable(rowModel)
  headerTable.setShowGrid(false)
  headerTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF)
  headerTable.setPreferredScrollableViewportSize(new Dimension(75, 0))
  headerTable.getColumnModel().getColumn(0).setPreferredWidth(75)

  headerTable.getColumnModel().getColumn(0).setCellRenderer(new TableCellRenderer() {
    override def getTableCellRendererComponent(x: JTable, value: Object, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int) = {
      val component = table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, value, false, false, -1, -2)
      component.asInstanceOf[JLabel].setHorizontalAlignment(SwingConstants.CENTER)
      component.setFont(component.getFont().deriveFont(Font.PLAIN))
      component
    }
  })

  val scrollpane = new JScrollPane()
  scrollpane.setRowHeaderView(headerTable)
  scrollpane.setViewportView(table)

  setLayout(new BorderLayout())
  add(scrollpane, BorderLayout.CENTER)

  val unitCalc = new JPanel()
  unitCalc.setLayout(new GridLayout(1, 4))
  unitCalc.add(new JLabel("HEX: "))
  val lblHex = new JLabel("0x0")
  unitCalc.add(lblHex)
  unitCalc.add(new JLabel("DEC: "))
  val lblDec = new JLabel("0")
  unitCalc.add(lblDec)
  add(unitCalc, BorderLayout.SOUTH)

  private val selectionListener = new ListSelectionListener {
    override def valueChanged(e: ListSelectionEvent) = {

      val lsm = e.getSource.asInstanceOf[ListSelectionModel]
      val start = lsm.getMinSelectionIndex
      val end = lsm.getMaxSelectionIndex + 1
      val len = end - start

      if (len <= 4 && model.env != null) {
        val ram = model.env.ram
        import ram._
        val idx = table.getSelectedRow * cols + table.getSelectedColumn
        if (idx > 0) try {
          val ramidx = model.env.addressToIndex(idx)
          val value: Int = len match {
            case 4 => getInt(ramidx)
            case 3 =>
              val intValue = ((getByte(ramidx + 2) & 0xFF) << 16) | ((getByte(ramidx + 1) & 0xFF) << 8) | (getByte(ramidx).toInt & 0xFF)
              intValue & 0xFFFFFF
            case 2 => getShort(ramidx).toInt & 0xFFFF
            case _ => getByte(ramidx).toInt & 0xFF
          }
          lblHex.setText(f"0x$value%08X")
          lblDec.setText(s"$value")
          unitCalc.repaint()
        } catch {
          case e: RuntimeException =>
            if (e.getMessage startsWith "Invalid heapadress:") {}
            else throw e
        }
      }
    }
  }

  table.getSelectionModel.addListSelectionListener(selectionListener)
  table.getColumnModel.getSelectionModel.addListSelectionListener(selectionListener)

  def changed(idx: Int, value: Byte) =
    SwingUtilities.invokeLater(runnable {
      table.getModel.setValueAt(f"$value%02X", idx % rows, idx / rows)
      table.repaint()
    })

}

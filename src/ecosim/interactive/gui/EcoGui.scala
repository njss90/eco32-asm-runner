/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive.gui

import java.awt._
import java.awt.event._
import java.io._
import GuiConstants._
import ecosim.Environment
import ecosim.gfx.EcoWindow
import ecosim.instructions.Instr
import ecosim.interactive.EcoModel
import ecosim.util._
import javax.swing._
import ScrollPaneConstants._
import JSplitPane._
import javax.imageio.ImageIO
import ecosim.Constants._

private[gui] object GuiConstants {
  val buttonSize = 16

  var editorFont = new Font("Consolas", Font.PLAIN, 14)

  var editor: DebugPane = null
  var model: EcoModel = null

  val smoothBlack = new Color(58, 58, 58)
  val lightBlue = new Color(232, 242, 254)
  val lightGreen = new Color(198, 219, 174)
  val commentGreen = new Color(63, 127, 98)
  val valueBlue = new Color(0, 0, 192)
  val registerGray = new Color(0, 0, 0)
  val pointerMaroon = new Color(95, 3, 26)
  val labelGray = new Color(158, 158, 158)

  private[gui] val ecoWindow = new EcoWindow()
  ecoWindow.setVisible(false)
}

class EcoGui(model: EcoModel) {
  private var frame: JFrame = null
  private var menu: JMenuBar = null
  private var settingsWindow: SettingsWindow = null

  private var play: JButton = null
  private var stop: JButton = null
  private var step: JButton = null

  private var openedFile: File = null
  private var registerPane: RegistersPane = null
  private var heapPane: HeapPane = null
  private var console: ConsoleComponent = null

  GuiConstants.model = model

  def handleReRun() =
    if (model.env != null && !model.env.running && !model.executionHalted) {
      model.env = null
      model.executionHalted = false
    }

  private[gui] def initRun() = {
    val lines = normalizeLines(editor.getText)
    val (labels, importedLabels) = allLabels(lines)
    val instructions = Instr.parseAll(importedLabels, labels, lines)

    val oldip =
      if (model.env != null) Some(model.env.ip)
      else None

    model.env =
      if (model.env != null && model.executionHalted)
        model.env
      else Environment(model.kbSize,
        if (model.graphicsEnabled) Some(ecoWindow) else None,
        instructions)

    model.env.registerChangedListener = registerPane.changed
    model.env.heapChangedListener = heapPane.changed

    if (!model.executionHalted) {
      //model.env.ip = findMain(labels)
      model.env.ip = 0
    } else if (oldip.isDefined) {
      model.env.ip = oldip.get
    }

    if (!model.executionHalted) {
      console.clearBuffers()
    }

    model.labels = labels
    model.lines = lines
  }

  SwingUtilities.invokeLater(runnable {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
    } catch { case e: Exception => }

    frame = new JFrame(editorName)
    settingsWindow = new SettingsWindow(frame)
    menu = new JMenuBar()

    frame.setJMenuBar(menu)
    frame.getContentPane.setPreferredSize(new Dimension(900, 560))
    frame.pack()

    frame.setBackground(Color.WHITE)
    frame.setForeground(smoothBlack)
    frame.getContentPane.setBackground(Color.WHITE)
    frame.getContentPane.setForeground(smoothBlack)

    frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)

    // JMenuBar
    val fileMenu = new JMenu("File")
    val openItem = new JMenuItem("Open ...")
    val saveItem = new JMenuItem("Save ...")
    openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK))
    saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK))
    fileMenu.add(openItem)
    fileMenu.add(saveItem)
    menu.add(fileMenu)

    openItem.addActionListener(new ActionListener() {
      override def actionPerformed(event: ActionEvent): Unit = {
        if (model.openFileChanged) {
          import JOptionPane._
          val res = showConfirmDialog(null, "Do you want to save your changes?", "Information", YES_NO_CANCEL_OPTION, QUESTION_MESSAGE)
          if (res == YES_OPTION) {
            if (openedFile ne null) {
              putText(openedFile, editor.getText)
              model.openFileChanged = false
            }
          } else if (res == NO_OPTION) {
            editor.setText("")
            model.openFileChanged = false
          }
        }
        if (!model.openFileChanged) {
          val chooser = new JFileChooser()
          chooser.setMultiSelectionEnabled(false)
          chooser.setFileFilter(new javax.swing.filechooser.FileFilter {
            override def accept(f: File): Boolean = f.isDirectory() || f.getName.endsWith(".asm")
            override def getDescription() = "*.asm"
          })
          val result = chooser.showOpenDialog(frame)

          if (result == JFileChooser.APPROVE_OPTION) {
            openedFile = chooser.getSelectedFile
            val text = getText(openedFile.getAbsolutePath)
            editor.setText(text)
            initRun()
            model.openFileChanged = false
            model.executionHalted = false
          }
        }
      }

    })

    saveItem.addActionListener(new ActionListener() {
      override def actionPerformed(event: ActionEvent): Unit = {
        if (model.openFileChanged) {
          if (openedFile == null) {
            val chooser = new JFileChooser()
            chooser.setMultiSelectionEnabled(false)
            val result = chooser.showSaveDialog(null)

            if (result == JFileChooser.APPROVE_OPTION) {
              openedFile = chooser.getSelectedFile
            }
          }
          if (openedFile ne null) {
            putText(openedFile, editor.getText)
            model.openFileChanged = false
          }
        }
      }
    })

    frame.addWindowListener(new WindowAdapter() {
      override def windowClosing(e: WindowEvent) = {
        if (model.openFileChanged) {
          import JOptionPane._
          val res = showConfirmDialog(null, "Do you want to save your changes?", "Information", YES_NO_CANCEL_OPTION, QUESTION_MESSAGE)
          if (res == YES_OPTION) {
            if (openedFile == null) {
              val chooser = new JFileChooser()
              chooser.setMultiSelectionEnabled(false)
              val result = chooser.showSaveDialog(null)

              if (result == JFileChooser.APPROVE_OPTION) {
                openedFile = chooser.getSelectedFile
              }
            }
            if (openedFile != null) {
              putText(openedFile, editor.getText)
              model.openFileChanged = false
              model.executionHalted = false
            }
          } else if (res == NO_OPTION) {
            model.openFileChanged = false
            model.executionHalted = false
          }
        }

        if (!model.openFileChanged) {
          frame.setVisible(false)
          frame.dispose()
          System.exit(0)
        }
      }
    })

    val windowMenu = new JMenu("Window")
    val settingsItem = new JMenuItem("Preferences")
    settingsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK))
    windowMenu.add(settingsItem)
    menu.add(windowMenu)

    menu.add(Box.createHorizontalGlue())

    editor = new DebugPane(this, model)

    console = new ConsoleComponent()
    System.setIn(console.getIn())
    System.setOut(console.getOut())

    val noWrapEditor = new JPanel(new BorderLayout())
    noWrapEditor.add(editor)

    val scrollSource = new JScrollPane(noWrapEditor, VERTICAL_SCROLLBAR_AS_NEEDED, HORIZONTAL_SCROLLBAR_AS_NEEDED)
    scrollSource.getVerticalScrollBar().setUnitIncrement(16)
    scrollSource.setBorder(null)

    val textArea = new JPanel(new BorderLayout(0, 0))
    textArea.setBorder(null)
    textArea.add(scrollSource, BorderLayout.CENTER)
    textArea.add(editor.scrollLine, BorderLayout.WEST)

    val editorConsoleSplit = new JSplitPane(VERTICAL_SPLIT, true, textArea, console)
    editorConsoleSplit.setBorder(null)
    editorConsoleSplit.setDividerLocation(400)

    registerPane = new RegistersPane(model)
    heapPane = new HeapPane(model)

    val registersHeapSplit = new JSplitPane(VERTICAL_SPLIT, true, heapPane, registerPane)
    registersHeapSplit.setBorder(null)
    registersHeapSplit.setDividerLocation(400)

    val editorRestSplit = new JSplitPane(HORIZONTAL_SPLIT, true, editorConsoleSplit, registersHeapSplit)
    editorRestSplit.setBorder(null)
    editorRestSplit.setDividerLocation(500)

    editor.addKeyListener(new KeyAdapter() {
      override def keyTyped(e: KeyEvent) = {
        model.openFileChanged = true
      }
    })

    scrollSource.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      override def adjustmentValueChanged(e: AdjustmentEvent) {
        editor.scroll(e.getAdjustable().getValue())
      }
    })

    frame.getContentPane.add(editorRestSplit)

    def createIconButton(name: String) = {
      val btn = new JButton()
      val icon = ImageIO.read(asset(name + ".png")).getScaledInstance(buttonSize, buttonSize, Image.SCALE_SMOOTH)
      btn.setIcon(new ImageIcon(icon))
      btn.setActionCommand(name)
      btn.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5))
      btn.setFocusPainted(false)
      btn.addActionListener(editor)
      btn
    }

    step = createIconButton("step")
    step.setToolTipText("step-through")
    menu.add(step)
    play = createIconButton("play")
    play.setToolTipText("run app")
    menu.add(play)
    stop = createIconButton("stop")
    stop.setToolTipText("stop")
    stop.setEnabled(false)
    menu.add(stop)

    val graphics = new JCheckBox("Graphics Enabled")
    graphics.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5))
    graphics.setToolTipText("enable graphics-window")
    graphics.addActionListener(new ActionListener() {
      override def actionPerformed(event: ActionEvent): Unit = {
        model.graphicsEnabled = graphics.isSelected()
      }
    })

    menu.add(graphics)

    settingsItem.addActionListener(new ActionListener() {
      override def actionPerformed(event: ActionEvent): Unit = {
        settingsWindow.show()
      }
    })

    frame.setVisible(true)
  })

  def setRunning() = {
    play.setEnabled(false)
    step.setEnabled(false)
    stop.setEnabled(true)
  }

  def resetButtons(debug: Boolean = false) = {
    play.setEnabled(true)
    step.setEnabled(true)
    stop.setEnabled(debug)
  }

}

private final class SettingsWindow(parent: JFrame) {
  private val frame = new JFrame(editorPreferences)
  private val tabs = new JTabbedPane()
  private val general = new JPanel(new FlowLayout(FlowLayout.LEFT))
  private val editor = new JPanel(new FlowLayout(FlowLayout.LEFT))
  private val buttonBar = new JPanel(new FlowLayout(FlowLayout.RIGHT))

  frame.getContentPane.setPreferredSize(new Dimension(540, 420))
  frame.pack()

  tabs.addTab("General", general)

  general.add(new JLabel("Execution Speed"))
  val speedSpinner = new JSpinner(new SpinnerNumberModel(1000, 1, 1000, 1))
  general.add(speedSpinner)

  tabs.addTab("Editor", editor)

  val fontModel = ((8 to 12) ++ (14 to 28 by 2) ++ Array(36, 48, 72, 96)).toArray
  val fontPicker = new JComboBox(new java.util.Vector[Int]())
  fontModel foreach fontPicker.addItem
  fontPicker.setSelectedItem(editorFont.getSize)

  editor.add(new JLabel("Font Size:"))
  editor.add(fontPicker)

  frame.getContentPane.add(tabs)
  frame.getContentPane.add(buttonBar, BorderLayout.SOUTH)

  val acceptButton = new JButton("Accept")
  val cancelButton = new JButton("Cancel")
  buttonBar.add(acceptButton)
  buttonBar.add(cancelButton)

  cancelButton.addActionListener(new ActionListener() {
    override def actionPerformed(event: ActionEvent) = hide()
  })

  acceptButton.addActionListener(new ActionListener() {
    override def actionPerformed(event: ActionEvent) {
      val size = fontPicker.getSelectedItem.asInstanceOf[Int]
      editorFont = editorFont.deriveFont(size.toFloat)
      GuiConstants.editor.reset()
      val execspeed = speedSpinner.getValue.asInstanceOf[Int]
      if (execspeed != model.executionSpeed) {
        model.executionSpeed = execspeed
      }
      hide()
    }
  })

  frame.setBackground(Color.WHITE)
  frame.setForeground(GuiConstants.smoothBlack)
  frame.getContentPane.setBackground(Color.WHITE)
  frame.getContentPane.setForeground(GuiConstants.smoothBlack)
  frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)

  def show(): Unit = {
    parent.setEnabled(false)
    frame.requestFocus()
    frame.setVisible(true)
  }

  def hide(): Unit = {
    frame.setVisible(false)
    parent.setEnabled(true)
    parent.requestFocus()
  }

  frame.addWindowListener(new WindowAdapter() {
    override def windowClosing(event: WindowEvent): Unit = {
      hide()
    }
  })

}

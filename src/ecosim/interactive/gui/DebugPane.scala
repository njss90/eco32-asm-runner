/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive.gui

import javax.swing.JTextPane
import javax.swing.text.StyledDocument
import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import javax.swing.text.DefaultStyledDocument
import ecosim.interactive.EcoModel
import ecosim.util._
import java.util.concurrent.Executors
import java.util.concurrent.Future
import GuiConstants._
import javax.swing.SwingUtilities
import java.awt.Color
import javax.swing.JScrollPane
import javax.swing.ScrollPaneConstants._
import scala.annotation.meta.beanGetter
import javax.swing.text.DefaultHighlighter
import javax.swing.text.BadLocationException
import java.util.concurrent.ThreadFactory
import java.lang.Thread.UncaughtExceptionHandler
import ecosim.Environment
import javax.swing.text.Utilities

class DebugPane(gui: EcoGui, model: EcoModel)
  extends JTextPane(new DefaultStyledDocument())
  with ActionListener {

  val textLineNumberAndBreakPoint = new BreakPointPainter(this, new Color(100, 100, 150))
  textLineNumberAndBreakPoint.setStartIndex(0)
  textLineNumberAndBreakPoint.setUpdateFont(true)
  textLineNumberAndBreakPoint.setMinimumDisplayDigits(3)
  textLineNumberAndBreakPoint.setCurrentLineForeground(registerGray)
  textLineNumberAndBreakPoint.setForeground(labelGray)
  textLineNumberAndBreakPoint.setBorderEnabled(false)

  val scrollLine = new JScrollPane(textLineNumberAndBreakPoint, VERTICAL_SCROLLBAR_NEVER, HORIZONTAL_SCROLLBAR_NEVER)
  scrollLine.setBorder(null)

  val linePainter = new LinePainter(this, lightBlue)

  val highlightPainter = new DefaultHighlighter.DefaultHighlightPainter(lightGreen)

  class StepTask extends Runnable {
    import model._

    override def run(): Unit = {
      if (!env.running) {
        gui.resetButtons(false)
        SwingUtilities.invokeLater(runnable {
          getHighlighter().removeAllHighlights()
        })
        return
      }

      runLine(env)
      lastRepaint = now()
      update(env.ip + 1)
      executionHalted = true
    }
  }

  class RunTask extends Runnable {
    import model._
    
    def canRun() =
      env != null && env.running && !Thread.interrupted() && (env.ip + 1) < env.instructions.length

    override def run(): Unit = {
      env.ip -= 1
      val timeout = 1000 - executionSpeed
      if (executionHalted) {
        env.ip += 1
        update(env.ip + 1)
        executionHalted = false
      }
      if (!env.running) {
        gui.resetButtons(false)
        SwingUtilities.invokeLater(runnable {
          getHighlighter().removeAllHighlights()
        })
        return
      }

      var breakpoint = false
      while (canRun() && !breakpoint) {
        runLine(env)
        breakpoint = isBreakPoint(env.ip + 1)
        if (timeout > 0) try Thread.sleep(timeout) catch {
          case e: InterruptedException =>
            Thread.currentThread().interrupt()
        }
      }
      if (!env.running) {
        breakpoint = false
      }
      gui.resetButtons(breakpoint)
      if (!breakpoint) {
        SwingUtilities.invokeLater(runnable {
          getHighlighter().removeAllHighlights()
        })
      } else {
        lastRepaint = now()
        update(env.ip + 1)
      }
      executionHalted = breakpoint
      if (future ne null) {
        future.cancel(true)
        future = null
      }
    }

    private def isBreakPoint(line: Int): Boolean =
      breakpoints() contains line

  }

  private def now() = System.currentTimeMillis()
  private var lastRepaint = now()
  
  private def runLine(env: Environment) =
    if ((env.ip + 1) < env.instructions.length) {
    	env.ip += 1
      if ((now() - lastRepaint) > 33) {
        lastRepaint = now()
        update(env.ip + 1)
      }
      val instruction = env.instructions(env.ip)
      if (instruction ne null) {
        instruction.run(env)
      }
    } else {
      env.ip = 0
    }

  private def update(line: Int) = {
    val caret = getCaretForLine(line)
    if (caret > -1) {
      SwingUtilities.invokeLater(runnable {
        setCaretPosition(caret)
        val element = getDocument().getParagraphElement(caret)
        val highlighter = getHighlighter()
        highlighter.removeAllHighlights()
        try {
          highlighter.addHighlight(element.getStartOffset(), element.getEndOffset(), highlightPainter)
        	repaint()
        } catch { case e: BadLocationException => }
      })
    }
  }

  private def getCaretForLine(line: Int) =
    if (line < 0) -1 else {
      var lctr = 0
      var idx = 0
      try {
        val len = getDocument().getLength()
        val text = getDocument().getText(0, len)
        while ((lctr < line) && (idx < len)) {
          if (text.charAt(idx) == '\n') lctr += 1
          idx += 1
        }
      } catch { case e: BadLocationException => }
      idx
    }

  private val executor = Executors.newFixedThreadPool(1, new ThreadFactory() {
    override def newThread(r: Runnable): Thread = {
      val thread = new Thread(r)
      thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
        override def uncaughtException(thread: Thread, t: Throwable) = {
          t.printStackTrace()
        }
      })
      thread
    }
  })
  @volatile
  var future: Future[_] = null

  @inline
  private def breakpoints() =
    textLineNumberAndBreakPoint.getBreakPoints

  override def actionPerformed(e: ActionEvent): Unit =
    e.getActionCommand match {
      case "step" =>
        gui.handleReRun()
        gui.resetButtons(true)
        gui.initRun()
        model.env.frame foreach { _.setVisible(model.graphicsEnabled) }
        model.env.frame foreach { _.env = model.env }
        executor.submit(new StepTask())
      case "stop" =>
        gui.resetButtons()
        if (future ne null) {
          future.cancel(true)
          future = null
        }
        model.executionHalted = false
        getHighlighter().removeAllHighlights()
      case "play" =>
        gui.handleReRun()
        gui.initRun()
        model.env.frame foreach { _.setVisible(model.graphicsEnabled) }
        model.env.frame foreach { _.env = model.env }
        gui.setRunning()
        future = executor.submit(new RunTask())
      case _ =>
    }

  def reset() = SwingUtilities.invokeLater(runnable {
    setFont(editorFont)
    textLineNumberAndBreakPoint.setFont(editorFont)
  })

  def scroll(amt: Int) =
    scrollLine.getVerticalScrollBar().setValue(amt)

  override def getDocument(): DefaultStyledDocument =
    super.getDocument().asInstanceOf[DefaultStyledDocument]

  reset()

}

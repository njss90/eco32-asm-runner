/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive.gui

import javax.swing._
import ecosim.interactive.EcoModel
import ecosim._
import ecosim.util._
import java.awt.GridLayout

class RegistersPane(model: EcoModel) extends JPanel {
  this.setLayout(new GridLayout(8, 4))
  val registerLabels = for (i <- 0 until Environment.registerCount) yield new JLabel("$" + i + ": 0")
  for (lbl <- registerLabels) this.add(lbl)

  def changed(index: Int, value: Int) =
    SwingUtilities.invokeLater(runnable {
      registerLabels(index).setText("$" + index + ": " + value)
      repaint()
    })
}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive

import ecosim.Environment

case class EcoModel(
  var env: Environment,
  var lines: Array[String],
  var labels: Map[String, Int],
  val kbSize: Int) {
  var executionSpeed = 1000
  var graphicsEnabled = false
  var openFileChanged = false
  var executionHalted = false
}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.interactive

import ecosim.interactive.gui.EcoGui
import ecosim.util._

object InteractiveSimulator {

  private var gui: EcoGui = null

  private val errorWithGui = { msg: String =>
    import javax.swing.JOptionPane._
    showMessageDialog(null, msg, "Error", ERROR_MESSAGE)
    throw new RuntimeException(msg)
  }

  def main(args: Array[String]): Unit = {
    error = errorWithGui

    var kbSize = 512

    var i = 0
    while (i < args.length) {
      if (args(i) == "-heap" && args(i + 1).matches("[0-9]+")) {
        kbSize = args(i + 1).toInt
        i += 1
      }
    }

    val model = EcoModel(null, null, null, kbSize)
    gui = new EcoGui(model)
  }

}

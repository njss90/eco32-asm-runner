/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.memory

import java.nio.ByteBuffer

class NativeMemory(val size: Int) extends MemoryLike {
  private val mem = ByteBuffer.allocateDirect(size)

  override def getByte(idx: Int): Byte = mem.get(idx)
  override def setByte(idx: Int, value: Byte) = mem.put(idx, value)
}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.memory

class VirtualMemory(val size: Int) extends MemoryLike {
  private val mem = new Array[Byte](size)

  override def getByte(idx: Int): Byte = mem(idx)
  override def setByte(idx: Int, value: Byte) = mem(idx) = value
}

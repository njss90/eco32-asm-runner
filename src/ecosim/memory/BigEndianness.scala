/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.memory

trait BigEndianness extends Endianness {
  self: MemoryLike =>

  override final def getShort(idx: Int): Short =
    (((getByte(idx) & 0xFF) << 8) | (getByte(idx + 1) & 0xFF)).toShort

  override final def setShort(idx: Int, value: Short): Unit = {
    setByte(idx + 0, ((value >> 8) & 0xFF).toByte)
    setByte(idx + 1, ((value) & 0xFF).toByte)
  }

  override final def getInt(idx: Int): Int =
    ((getByte(idx) & 0xFF) << 24) | ((getByte(idx + 1) & 0xFF) << 16) | ((getByte(idx + 2) & 0xFF) << 8) | (getByte(idx + 3) & 0xFF)

  override final def setInt(idx: Int, value: Int): Unit = {
    setByte(idx + 0, ((value >>> 24) & 0xFF).toByte)
    setByte(idx + 1, ((value >>> 16) & 0xFF).toByte)
    setByte(idx + 2, ((value >>> 8) & 0xFF).toByte)
    setByte(idx + 3, ((value) & 0xFF).toByte)
  }

  override final def getLong(idx: Int): Long =
    (getByte(idx).toLong << 56) | ((getByte(idx + 1) & 0xFFL) << 48) | ((getByte(idx + 2) & 0xFFL) << 40) | ((getByte(idx + 3) & 0xFFL) << 32) |
      ((getByte(idx + 4) & 0xFFL) << 24) | ((getByte(idx + 5) & 0xFFL) << 16) | ((getByte(idx + 6) & 0xFFL) << 8) | (getByte(idx + 7) & 0xFFL)

  override final def setLong(idx: Int, value: Long): Unit = {
    setByte(idx + 0, ((value >>> 56) & 0xFF).toByte)
    setByte(idx + 1, ((value >>> 48) & 0xFF).toByte)
    setByte(idx + 2, ((value >>> 40) & 0xFF).toByte)
    setByte(idx + 3, ((value >>> 32) & 0xFF).toByte)
    setByte(idx + 4, ((value >>> 24) & 0xFF).toByte)
    setByte(idx + 5, ((value >>> 16) & 0xFF).toByte)
    setByte(idx + 6, ((value >>> 8) & 0xFF).toByte)
    setByte(idx + 7, ((value) & 0xFF).toByte)
  }
}

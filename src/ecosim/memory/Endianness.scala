/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.memory

trait Endianness {
  self: MemoryLike =>

  def getShort(idx: Int): Short
  def setShort(idx: Int, value: Short): Unit

  def getInt(idx: Int): Int
  def setInt(idx: Int, value: Int): Unit

  def getLong(idx: Int): Long
  def setLong(idx: Int, value: Long): Unit

  final def getChar(idx: Int): Char =
    getByte(idx).toChar
  final def setChar(idx: Int, value: Char): Unit =
    setByte(idx, value.toByte)

  final def getFloat(idx: Int): Float =
    java.lang.Float.intBitsToFloat(getInt(idx))
  final def setFloat(idx: Int, value: Float): Unit =
    setInt(idx, java.lang.Float.floatToIntBits(value))

  final def getDouble(idx: Int): Double =
    java.lang.Double.longBitsToDouble(getLong(idx))
  final def setDouble(idx: Int, value: Double): Unit =
    setLong(idx, java.lang.Double.doubleToLongBits(value))
}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim.memory

trait MemoryLike {
  def getByte(idx: Int): Byte
  def setByte(idx: Int, value: Byte): Unit
}

/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

package object memory {

  type Ram = MemoryLike with Endianness

}

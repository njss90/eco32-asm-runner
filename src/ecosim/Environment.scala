/**
 * Copyright (c) 2015 Nicola Justus, Jan Sladek
 *
 * See the file LICENSE for copying permission.
 */

package ecosim

import ecosim.memory._
import util._
import Environment._
import java.util.concurrent.TimeUnit
import ecosim.gfx.EcoWindow
import ecosim.instructions.Instr

object Environment {
  val badRegisters = Array(1, 2, 3, 4, 5, 6, 7, 24, 26, 27, 28, 30)
  val stopExec = Integer.MIN_VALUE
  val registerCount = 32

  def isBadRegister(reg: Int) =
    (reg >= 1 && reg < 8) || (reg == 24) || (reg >= 26 && reg < 29) || (reg == 30)
}

case class Environment(
  kbSize: Int,
  var frame: Option[EcoWindow],
  instructions: Array[Instr]) {

  private val startTime = System.currentTimeMillis()

  private val size = kbSize << 10

  private val registers = Array.fill(registerCount)(0)

  val ram: Ram = new VirtualMemory(size) with BigEndianness with HeapReadListener
  private var maxHeapIndex = 0
  private var minStackIndex = size

  private val stackPointer = 29
  /** instruction pointer */
  var ip: Int = 0

  var running = true

  var registerChangedListener: (Int, Int) => Unit = null

  trait HeapReadListener extends MemoryLike {
    override abstract def setByte(idx: Int, value: Byte) = {
      super.setByte(idx, value)
      if (heapChangedListener ne null) {
        heapChangedListener(idx, value)
      }
    }
  }
  var heapChangedListener: (Int, Byte) => Unit = null

  writeRegister(25, size)
  writeRegister(29, size)
  writeRegister(31, stopExec)

  /** Sets the heapsize if there's enough space.
    * Exit with Out-of-memory-error otherwise.
    */
  def setHeapIdx(idx:Int) = if(idx < minStackIndex) {
    //save highest index
    if(idx > maxHeapIndex)
      maxHeapIndex = idx
  } else error(s"Out-of-memory-error, index $idx > SP $minStackIndex")

  /** Sets the stacksize if there's enough space.
    * Exit with stackoveflow otherwise.
    */
  def setStackIdx(idx:Int) = if(idx > maxHeapIndex) {
    minStackIndex = idx
  } else error(s"Stackoverflow: Value $idx < Heapsize $maxHeapIndex")

  @inline
  def addressToIndex(adr: Int) = {
    if (adr < 0 || adr >= size) {
      error(f"Invalid heapadress: 0x$adr%08X")
    } else adr
  }

  def readRegister(idx: Int): Int =
    if (isBadRegister(idx)) {
      error(s"Illegal access on register: $idx!")
    } else if (idx == 0) 0
    else {
      registers(idx)
    }

  def writeRegister(idx: Int, value: Int): Unit =
    if (isBadRegister(idx)) {
      error(s"Illegal access on register: $idx!")
    } else if (idx != 0) {
      registers(idx) = value

      if (idx == stackPointer) {
        setStackIdx(value)
      }

      if (registerChangedListener ne null) {
        registerChangedListener(idx, value)
      }
    }

  def timeRunning() = TimeUnit.SECONDS.convert(
    System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS).toInt

  /** returns the n-th function argument (always 4 bytes) */
  def getArg(n: Int) = {
    val fp = readRegister(25)
    val ramidx = addressToIndex(fp + n * 4)
    ram.getInt(ramidx)
  }

  def doReturn() = {
    val ret = readRegister(31)
    ip = ret
  }

  def saveRegisters(localsize: Int = 0) = {
    val fp = readRegister(25)
    var sp = readRegister(29)
    val ret = readRegister(31)
    writeRegister(29, sp - (8 + localsize))
    writeRegister(25, sp)
    sp = readRegister(29)
    ram.setInt(addressToIndex(sp), ret)
    ram.setInt(addressToIndex(sp + 4), fp)
  }

  def restoreRegisters(localsize: Int = 0) = {
    val fp = readRegister(25)
    val oldret = ram.getInt(addressToIndex(fp - (8 + localsize)))
    val oldfp = ram.getInt(addressToIndex(fp - (4 + localsize)))
    writeRegister(29, fp)
    writeRegister(25, oldfp)
    writeRegister(31, oldret)
  }

  def exit() = {
    frame foreach (_.exit())
    running = false
  }

}
